Ansible-Playbooks
=====
This repo holds all ansible playbooks used by FB-IT to configure linux servers or desktops
----------------
Prerequisites:
You must have sudo rights on the server to execute.
Ansible installed on your client

To use a playbook on your own machine:

> sudo ansible-playbook printing-ubuntu.yml -i "localhost," -c local

To use on a playbook on single node (scomp1000.wurnet.nl):

> ansible-playbook -s printing-redhat.yml -i scomp1000.wurnet.nl, --ask-sudo-pass -k

To use on a playbook on multiple nodes:

> ansible-playbook -s printing-redhat.yml -i scomp1000.wurnet.nl,scomp1001.wurnet.nl --ask-sudo-pass -k

To get a more verbose output:

> ansible-playbook -s printing-redhat.yml -i scomp1000.wurnet.nl, --ask-sudo-pass -k -v
